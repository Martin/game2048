from game2048.grid_2048 import *

def read_player_command():
    move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
    while move !="g" or move !="d" or move !="h"or move !="b":
        move = input("Cette commande n'est pas valide, entrez une autre commande (g (gauche), d (droite), h (haut), b (bas)):")
    return move

def read_size_grid():
    taille=input("Entrez la taille de la grille:")
    return int(taille)

def read_theme_grid():
    theme=("Entrez le thème choisi (0 (Nombres), 1 (Éléments chimiques), 3 (Lettres):")
    return '"'+str(theme)+'"'

if __name__ == '__main__':
    game_play()
    exit(1)

