from pytest import *
from game2048.textual_2048 import *

def test_mock_input_return(monkeypatch):
    def mock_return():
        return random.choice(["h","b","d","g"])
    monkeypatch.setattr("builtins.input",mock_return())
    move=read_player_command()
    assert move in ["h","b","d","g"]
    assert move != "t"

