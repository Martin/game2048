import random as rd
THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}
import copy

def create_grid(x=4):                           #créer un grille vide
    game_grid = []
    for i in range(0,x):
        game_grid.append([0 for i in range(0,x)])
    return game_grid

def get_value_new_tile():                       #donne les valeurs possibles pour l'algo précédent
    a=rd.choices([2,4],weights=[0.9,0.1],k=1)
    return a[0]

def get_all_tiles(L):                           #donne toutes les valeurs présentent dans la grile
    J=[]
    for k in L:
        for i in k:
            if i==' ':
                J.append(0)
            else:
                J.append(i)
    return J

def get_empty_tiles_positions(grille):               #donne l'emplacement des cases vides de la grille
    l=len(grille)
    Position=[]
    for i in range(0,l):
        for j in range(0,l):
            if grille[i][j]==0 or grille [i][j]==' ':
                Position.append((i,j))
    return(Position)

def grid_get_value(L,x,y):                      #donne la valeur d'une case de la grille
    if L[x][y]==' ':
        return 0
    return L[x][y]

def get_new_position(L):                        #donne une position au hasard dans laquelle on peut ajouter une nouvelle valeur
    J=get_empty_tiles_positions(L)
    k=rd.choice(J)
    return k

def grid_add_new_tile(L):                       #ajouter une valeur à notre grille à partir d'un emplacement vide
    x,y=get_new_position(L)
    L[x][y]=get_value_new_tile()
    return L

def init_game(x=4):                             #initiallise le jeu
    L=create_grid(x)
    J=grid_add_new_tile(L)
    K=grid_add_new_tile(J)
    return K

def grid_to_string(L,x):                        #crée la grille
    G='''
'''

    for i in range(x):
        G+=x*''' ==='''

        G+='''
'''
        for j in range(x):
            a=str(L[i][j])
            G+='''| '''+a+""" """
        G+='''|'''
        G+='''
'''
    G+=x*''' ==='''
    G+='''
    '''
    return G

def long_value(L):                              #donne la plus grande longueur des éléments de la liste
    m=0
    for i in range(len(L)):
        for j in range(len(L[i])):
            if len(str(L[i][j]))>m:
                m=len(str(L[i][j]))
    return m

def grid_to_string_with_size(L,n=4):              #crée la grille en prenant en compte la taille
    m=long_value(L)
    G='''
'''
    for i in range(n):
        G+=n*(''' ='''+m*'''='''+'''=''')
        G+='''
'''
        for j in range(n):
            a=str(L[i][j])
            G+='''| '''+a+""" """*(m-len(a)+1)
        G+='''|'''
        G+='''
'''
    G+=n*(''' ='''+m*'''='''+'''=''')
    G+='''
    '''
    return G

def long_value_with_theme(L,d):                     #Longueur avec le thème en plus
    a=L[0][0]

    for i in range(len(L)):
        for j in range(len(L[i])):
            if L[i][j]>a:
                a=L[i][j]
    return len(d[a])

def grid_to_string_with_size_and_theme(L,d,n=4):      #crée la grille avec le thème et la longueur
    m=long_value_with_theme(L,d)
    G='''
'''
    for i in range(n):
        G+=n*(''' ='''+m*'''='''+'''=''')
        G+='''
'''
        for j in range(n):
            a=str(d[L[i][j]])
            G+='''| '''+a+""" """*(m-len(a)+1)
        G+='''|'''
        G+='''
'''
    G+=n*(''' ='''+m*'''='''+'''=''')
    G+='''
    '''
    return G

def move_row_left(L):               #mouvement à gauche
    k=0
    for i in range(len(L)-1):
        while L[i]==0 and k<len(L):
            k=k+1
            for j in range(i,len(L)-1):
                L[j],L[j+1]=L[j+1],L[j]
    for i in range(len(L)-1):
        if L[i]==L[i+1]:
            L[i]=2*L[i+1]
            L[i+1]=0
    for i in range(len(L)-1):
        while L[i]==0 and k<len(L):
            k=k+1
            for j in range(i,len(L)-1):
                L[j],L[j+1]=L[j+1],L[j]
    return L

def move_row_right(L):              #mouvement à droite
    G=[0 for i in range(len(L))]
    for i in range(len(L)):
        G[i]=L[len(L)-1-i]
    K=move_row_left(G)
    J=[0 for i in range(len(L))]
    for i in range(len(K)):
        J[i]=K[len(K)-1-i]
    return J

def move_grid(grid,d):                  #fais le mouvement demandé
    if d=="g":
        for i in range(len(grid)):
            grid[i]=move_row_left(grid[i])
    if d=="d":
        for i in range(len(grid)):
            grid[i]=move_row_right(grid[i])
    if d=="b":
        for i in range(len(grid)):
            L=[]
            for j in range(len(grid[i])):
                L.append(grid[j][i])
            K=move_row_right(L)
            for j in range(len(grid[i])):
                grid[j][i]=K[j]
    if d=="h":
        for i in range(len(grid)):
            L=[]
            for j in range(len(grid[i])):
                L.append(grid[j][i])
            K=move_row_left(L)
            for j in range(len(grid[i])):
                grid[j][i]=K[j]
    return grid

def is_grid_full(grid):             #dis si la grille est pleine
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j]==0:
                return False
    return True

def move_possible(grid):            #dis si le mouvement est possible
    L=["d","g","b","h"]
    G=[]
    for d in L:
        grid=copy.deepcopy(grid)
        grid=move_grid(grid,d)
        if not is_grid_full(grid):
            G.append(True)
        else:
            G.append(False)
    return G

def is_game_over(grid):                    #dis si le jeu est fini
    if move_possible(grid)==[False,False,False,False]:
        return "Game Over"

def get_grid_tile_max(grid):                #donne le plus grand élément de la grille
    m=0
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j]>=m:
                m=grid[i][j]
    return m

def is_won(grid):                           #dis si le jeu est gagné
    m=get_grid_tile_max(grid)
    if m>2047:
        return "Vous avez gagné le jeu !"
    return "Jouez un coup"

def random_play():
    L={0:"d", 1:"g", 2:"b", 3:"h"}
    grid=init_game(x=4)
    print(grid_to_string_with_size(grid,n=4))
    while is_game_over(grid)!="Game Over":
        move=move_possible(grid)
        next=[]
        for k in range(4):
            if move[k]:
                next.append(L[k])
        d=rd.choice(next)
        grid=move_grid(grid,d)
        if is_grid_full(grid):          #Dans le cas où après avoir joué, la grille est complète, on joue jusqu'à ce qu'on trouve un nouveau 0 dans la grille
            continue
        grid=grid_add_new_tile(grid)
        print(grid_to_string_with_size(grid,n=4))
    if is_won(grid)=="Vous avez gagné le jeu !":
        return is_won(grid)
    return "Game Over"

def ask_and_read_grid_size():
    taille=input("Entrez la taille de la grille:")
    return int(taille)

def ask_and_read_grid_theme():
    theme=("Entrez le thème choisi (0 (Nombres), 1 (Éléments chimiques), 3 (Lettres):")
    return '"'+str(theme)+'"'


def game_play():
    taille=int(input("Entrez la taille de la grille:"))
    theme=input("Entrez le thème choisi (0 (Nombres), 1 (Éléments chimiques), 2 (Lettres):")
    grille=init_game(taille)
    print(grid_to_string_with_size_and_theme(grille,THEMES[theme],taille))
    while is_game_over(grille)!="Game Over":
        move=input("Jouez:")                #on demande le move
        L=copy.deepcopy(grille)             #on sauvegarde la grille pour qu'elle ne soit pas modifiée
        G=move_grid(grille,move)
        if move in ["g","d","h","b"]:       #si il y a une faute de frappe on redemande
            if L==G:                        #On regarde si le mouvement a modifié la grille
                while L==G:                 #Tant que la grille n'est pas modifiée, on redemande un mouvement
                    move=input("Jouez:")
                    G=move_grid(G,move)
            grille=G                        #une fois que c'est bon, on change la grille
        else:
            continue
        if is_grid_full(grille):            #Idem que pour randm_play()
            print(grid_to_string_with_size_and_theme(grille,THEMES[theme],taille))
            continue
        grille=grid_add_new_tile(grille)
        print(grid_to_string_with_size_and_theme(grille,THEMES[theme],taille))
    if is_won(grille)=="Vous avez gagné le jeu !":
        return is_won(grille)
    return "Game Over"

