from tkinter import *
from game2048.grid_2048 import *
from functools import partial

taille=int(input("Taille de la grille:"))
grid = init_game(taille)
fenetre = Tk()
fenetre.title('2048')
fenetre.resizable(False, False)
fenetre.geometry("800x800-500+0")
fenetre2=Toplevel()
fenetre2.title('2048')
fenetre2.grid()


def color_frame(number) :
    TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                      16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                      128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                      1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                      8192: "#24ba63"}

    TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                      16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                      256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                      2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

    TILES_FONT = {"Verdana", 40, "bold"}
    return TILES_BG_COLOR[number],TILES_FG_COLOR[number]


def graphical_grid_init(game_grid) :            #Donne la grille initiale
    frames=[[] for i in range(taille)]
    labels=[[] for i in range(taille)]
    for i in range(taille) :
        for j in range(taille) :
            number=game_grid[i][j]
            bg_color,fg_color=color_frame(number)
            frames[i].append(Frame(fenetre,bd=1,relief='solid',width=800/taille, height=800/taille,bg=bg_color))   #on règle la taille d'une case en foction de la taille de la grille (ne fonctionne que pour des puissances de 2)
            frames[i][j].grid(row=i, column=j)
            frames[i][j].grid_propagate(0)
            if number==0 :
                number= ' '
            labels[i].append(Label(frames[i][j], text = number,padx=0 , pady= 0,bg=bg_color,fg=fg_color,font=("Verdana", 40, "bold"), anchor='se'))
            labels[i][j].grid()
    return frames,labels


def display_and_update_graphical_grid(grid,move) :
    def movement_grille(mouvement) :              #On met un argument inutile sinon l'algo ne fait pas le move
        grid_copy=copy.deepcopy(grid)
        move_grid(grid,move)
        if not is_grid_full(grid) and grid_copy != grid :
            grid_add_new_tile(grid)
        for i in range(taille):
            for j in range(taille) :
                number=grid[i][j]
                bg_color,fg_color=color_frame(number)
                frames[i][j].config(bg=bg_color)
                if number == 0 :
                    number= ' '
                labels[i][j].config(text=number,bg=bg_color,fg=fg_color)
    return movement_grille


frames,labels=graphical_grid_init(grid)
fenetre.bind('<KeyPress-Down>',display_and_update_graphical_grid(grid,'b'))             #correspon à key_pressend
fenetre.bind('<KeyPress-Up>',display_and_update_graphical_grid(grid,'h'))
fenetre.bind('<KeyPress-Right>',display_and_update_graphical_grid(grid,'d'))
fenetre.bind('<KeyPress-Left>',display_and_update_graphical_grid(grid,'g'))


text = StringVar(fenetre2)
label = Label(fenetre2, text='Taille de la grille')
entry_name = Entry(fenetre2, textvariable=text)
button = Button(fenetre2, text='clic')
label.grid(column=0, row=0)
entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)

fenetre.mainloop()


